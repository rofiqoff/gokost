package app.gokost.rofiqoff.com.gokost.Database;

/**
 * Created by RofiqoFF on 08/11/2016.
 */

public class BerandaList {

    private int imageFlag;
    private String namaKos;
    private String alamatKos;

    public BerandaList(int imageFlag, String namaKos, String alamatKos) {
        this.imageFlag = imageFlag;
        this.namaKos = namaKos;
        this.alamatKos = alamatKos;
    }

    public int getImageFlat() {
        return imageFlag;
    }

    public void setImageFlat(int imageFlat) {
        this.imageFlag = imageFlat;
    }

    public String getNamaKos() {
        return namaKos;
    }

    public void setNamaKos(String namaKos) {
        this.namaKos = namaKos;
    }

    public String getAlamatKos() {
        return alamatKos;
    }

    public void setAlamatKos(String alamatKos) {
        this.alamatKos = alamatKos;
    }
}
