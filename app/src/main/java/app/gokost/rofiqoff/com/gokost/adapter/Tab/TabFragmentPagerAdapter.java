package app.gokost.rofiqoff.com.gokost.adapter.Tab;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import app.gokost.rofiqoff.com.gokost.fragment.Identitas.DaftarFragment;
import app.gokost.rofiqoff.com.gokost.fragment.Identitas.LoginFragment;

/**
 * Created by RofiqoFF on 01/10/2016.
 */

public class TabFragmentPagerAdapter extends FragmentPagerAdapter {

    String[] title = new String[]{
            "LOGIN", "DAFTAR"
    };

    public TabFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new LoginFragment();

                break;
            case 1:
                fragment = new DaftarFragment();
                break;
            default:
                fragment = null;
                break;
        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position){
        return title[position];
    }

    @Override
    public int getCount() {
        return title.length;
    }
}
