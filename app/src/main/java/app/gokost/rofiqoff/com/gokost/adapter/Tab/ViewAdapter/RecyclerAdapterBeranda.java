package app.gokost.rofiqoff.com.gokost.adapter.Tab.ViewAdapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import app.gokost.rofiqoff.com.gokost.Database.BerandaList;
import app.gokost.rofiqoff.com.gokost.R;

/**
 * Created by RofiqoFF on 08/11/2016.
 */

public class RecyclerAdapterBeranda extends RecyclerView.Adapter<RecyclerAdapterBeranda.ViewHolder> {

    private Activity activity;
    private List<BerandaList> berandaLists;

    public RecyclerAdapterBeranda(Activity activity, List<BerandaList> berandaLists) {
        this.activity = activity;
        this.berandaLists = berandaLists;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.item_recycler_beranda, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.imageFlag.setImageResource(berandaLists.get(position).getImageFlat());
        viewHolder.txNamaKos.setText(berandaLists.get(position).getNamaKos());
        viewHolder.txAmalat.setText(berandaLists.get(position).getAlamatKos());
    }

    @Override
    public int getItemCount() {
        return (null != berandaLists ? berandaLists.size() : 0);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageFlag;
        private TextView txNamaKos;
        private TextView txAmalat;
        private View container;

        public ViewHolder(View view) {
            super(view);
            imageFlag = (ImageView) view.findViewById(R.id.flag);
            txNamaKos = (TextView) view.findViewById(R.id.txNama_kos);
            txAmalat = (TextView) view.findViewById(R.id.txAlamat_kos);
            container = view.findViewById(R.id.cardView_beranda);
        }
    }
}
