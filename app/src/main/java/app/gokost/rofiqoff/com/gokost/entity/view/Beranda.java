package app.gokost.rofiqoff.com.gokost.entity.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import app.gokost.rofiqoff.com.gokost.Database.BerandaList;
import app.gokost.rofiqoff.com.gokost.R;
import app.gokost.rofiqoff.com.gokost.adapter.Tab.ViewAdapter.RecyclerAdapterBeranda;

public class Beranda extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private RecyclerAdapterBeranda recyclerAdapterBeranda;
    private ArrayList<BerandaList> berandaLists;
    private Button buttonAkun;

    private TextView nav_nama;
    private TextView nav_alamat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beranda);
        toolbar = (Toolbar) findViewById(R.id.toolbarActivity);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("GO KOST");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_beranda);
        berandaLists = new ArrayList<>();

        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(Beranda.this);
        recyclerView.setLayoutManager(layoutManager);

        setDataView();
        recyclerAdapterBeranda = new RecyclerAdapterBeranda(Beranda.this, berandaLists);
        recyclerView.setAdapter(recyclerAdapterBeranda);

        buttonAkun = (Button) findViewById(R.id.akun);

        klikAkun();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setDataView() {
        berandaLists.add(new BerandaList(R.drawable.rumah, "Kosan Bu Lastri", "Jl Sumber Sari No 10B"));
        berandaLists.add(new BerandaList(R.drawable.rumah2, "Kosan Bu Pipit", "Jl Joyosuko Timur No 50B"));
        berandaLists.add(new BerandaList(R.drawable.rumah3, "Bu Suci Kosan", "Jln Bendungan Sutami no 10C, Sigura-gura"));
        berandaLists.add(new BerandaList(R.drawable.rumah4, "Putra Kosan", "jln sigura-gura, no24A"));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void klikAkun(){
        buttonAkun.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(Beranda.this, MainActivity.class));
                    }
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.beranda, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_beranda){
            // Handle the camera action
        } else if (id == R.id.nav_peta) {

        } else if (id == R.id.nav_kotakMasuk) {

        } else if (id == R.id.nav_pengaturan) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_abaout) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
