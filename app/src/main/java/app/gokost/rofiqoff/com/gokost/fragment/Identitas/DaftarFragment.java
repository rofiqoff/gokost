package app.gokost.rofiqoff.com.gokost.fragment.Identitas;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import app.gokost.rofiqoff.com.gokost.R;
import app.gokost.rofiqoff.com.gokost.entity.Pendaftaran.DaftarOwnerActivity;
import app.gokost.rofiqoff.com.gokost.entity.Pendaftaran.DaftarPenyewaActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class DaftarFragment extends Fragment {

    private Button btn_owner, btn_penyewa;
    private EditText input;


    public DaftarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_daftar, container, false);

        btn_owner = (Button) view.findViewById(R.id.tombol_daftar_pemilik);
        btn_penyewa = (Button) view.findViewById(R.id.tombol_daftar_pengekos);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btn_owner.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        startActivity(new Intent(getContext(), DaftarOwnerActivity.class));

                    }
                }
        );

        btn_penyewa.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Toast.makeText(getActivity(), "Penyewa", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getActivity(), DaftarPenyewaActivity.class));
                    }
                }
        );
    }

}
