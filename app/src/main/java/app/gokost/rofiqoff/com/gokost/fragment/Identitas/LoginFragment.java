package app.gokost.rofiqoff.com.gokost.fragment.Identitas;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import app.gokost.rofiqoff.com.gokost.R;
import app.gokost.rofiqoff.com.gokost.entity.view.Beranda;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    private EditText username;
    private EditText pass;
    private Button btn_login;
    private Button btn_Akun;
    private RadioGroup radioGroup;
    private RadioButton radioButton;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_login, container, false);

        username = (EditText) view.findViewById(R.id.edit_text_username);
        pass = (EditText) view.findViewById(R.id.edit_text_password);
        btn_login = (Button) view.findViewById(R.id.tombol_login);

        btn_Akun = (Button) view.findViewById(R.id.akun);

        radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);

        int selectedID = radioGroup.getCheckedRadioButtonId();
        radioButton = (RadioButton) view.findViewById(selectedID);

//        radioGroup.setOnCheckedChangeListener(
//                new RadioGroup.OnCheckedChangeListener() {
//
//                    @Override
//                    public void onCheckedChanged(RadioGroup group, int checkedId) {
//                        switch (checkedId) {
//                            case R.id.rbPenyewa: break;
//                            case R.id.rbOwner: break;
//                        }
//                    }
//                }
//        );

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        addListenerOnButton();
    }

    private void addListenerOnButton() {
        btn_login.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String sName = username.getText().toString();
                        String sPass = pass.getText().toString();

                        if (radioButton.getText().toString().equalsIgnoreCase("Penyewa")) {
                            if (sName.equalsIgnoreCase("Rofiqo")) {
                                if (sPass.equalsIgnoreCase("Rofiqo")) {
                                    Toast.makeText(getActivity(), "Selemat Datang " + radioButton.getText(), Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getContext(), Beranda.class));

//                                    btn_Akun.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            if (sName.equalsIgnoreCase("Fauzan")) {
                                if (sPass.equalsIgnoreCase("Fauzan")) {
                                    Toast.makeText(getActivity(), "Selemat Datang " + radioButton.getText(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                }
        );
    }
}